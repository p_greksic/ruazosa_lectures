package hr.fer.tel.ruazosa.lectures.net.retrofit

import android.util.Log

import java.io.IOException

import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import hr.fer.tel.ruazosa.lectures.net.RestInterface
import retrofit.RestAdapter

class RestRetrofit : RestInterface {
    private val service: LecturesService

    init {
        val baseURL = "http://" + RestFactory.BASE_IP + ":8080/api/"
        val retrofit = RestAdapter.Builder()
                .setEndpoint(baseURL)
                .build()

        service = retrofit.create(LecturesService::class.java)
    }

    override fun getListOfCourses(): List<ShortCourse>? {
        return service.listOfCourses
    }

    override fun getCourse(id: Long?): Course? {
        return service.getCourse(id)
    }

    override fun getCourseStudents(courseId: Long?): List<ShortPerson>? {
        return service.getCourseStudents(courseId)
    }

    override fun getListOfPersons(): List<ShortPerson>? {
        return service.listOfPersons
    }

    override fun getPerson(id: Long?): Person? {
        return service.getPerson(id)
    }

    override fun enrollPersonToCourse(personId: Long?, courseId: Long?): Boolean? {
        return service.enrollPersonToCourse(courseId,personId)
    }

    override fun disenrollPersonFromCourse(personId: Long?, courseId: Long?): Boolean? {
        return service.disenrollPersonFromCourse(courseId, personId)
    }

    override fun deletePerson(id: Long?): Boolean? {
        val allCourses: List<ShortCourse> = service.listOfCourses
        val personToDelete = service.getPerson(id)
        // first disenroll student from all courses
        for(c in allCourses){
            val courseStudents: List<ShortPerson> = service.getCourseStudents(c.id)
            if(courseStudents != null && courseStudents.isNotEmpty()) {
                for(s in courseStudents) {
                    if (s.id == personToDelete.id) {
                        service.disenrollPersonFromCourse(c.id, personToDelete.id)
                        break
                    }
                }
            }
        }
        // delete student
        return service.deletePerson(id)
    }

    override fun postPerson(p: Person?): Boolean? {
        return service.postPerson(p)
    }

    override fun updatePerson(p: Person?): Boolean? {
        var personToUpdate: Person? = null
        var attendedCourses: MutableList<ShortCourse>? = null
        val allCourses: List<ShortCourse> = service.listOfCourses
        var newPersonId: Long? = null
        if(p != null){
            personToUpdate = service.getPerson(p.id)
            attendedCourses = mutableListOf()
            // first disenroll student from all courses (basically delete but we save courses)
            for(c in allCourses){
                val courseStudents = service.getCourseStudents(c.id)
                if(courseStudents != null && courseStudents.isNotEmpty()){
                    for(s in courseStudents) {
                        if(s.id == personToUpdate.id) {
                            attendedCourses!!.add(c)
                            service.disenrollPersonFromCourse(c.id, personToUpdate.id)
                        }
                    }
                }
            }
            // after disenrolling student from all courses, delete him
            service.deletePerson(personToUpdate.id)

            // then post person again, but the problem is id of person on server
            service.postPerson(p)

            // find newPersonId manually
            val allPersons = service.listOfPersons
            for(sp in allPersons){
                val person = service.getPerson(sp.id)
                if(person.firstName == p.firstName && person.lastName == p.lastName && person.phone == p.phone && person.room == p.room){
                    newPersonId = person.id
                    break
                }
            }
            // then enroll him to all classes again
            for(c in attendedCourses){
                service.enrollPersonToCourse(c.id,newPersonId)
            }
            return true
        }
        return false
    }
}
