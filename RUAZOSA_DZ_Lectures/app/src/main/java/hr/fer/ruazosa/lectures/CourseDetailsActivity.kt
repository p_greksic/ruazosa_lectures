package hr.fer.ruazosa.lectures

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory

class CourseDetailsActivity : AppCompatActivity() {

    private var course: Course? = null
    private var courseName: TextView? = null
    private var courseDescription: TextView? = null
    private var teacher: TextView? = null
    private var listCourseStudentsButton: Button? = null
    private var courseStudentsListView: ListView? = null
    private var addStudentsButton: Button? = null
    private var removeStudentsButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_details)

        courseName = findViewById(R.id.courseName) as TextView
        courseDescription = findViewById(R.id.courseDescription) as TextView
        teacher = findViewById(R.id.courseTeacher) as TextView
        listCourseStudentsButton = findViewById(R.id.listCourseStudentsButton) as Button
        courseStudentsListView = findViewById(R.id.courseStudentsListView) as ListView
        addStudentsButton = findViewById(R.id.addStudentsButton) as Button
        removeStudentsButton = findViewById(R.id.removeStudentsButton) as Button

        val shortCourse = intent.getSerializableExtra("course") as ShortCourse

        LoadShortCourseTask().execute(shortCourse)

        courseStudentsListView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            // when student is clicked Activity for editing their params is started
            val itemAtPosition = parent.getItemAtPosition(position)
            val shortPerson = itemAtPosition as ShortPerson
            val intent = Intent(this@CourseDetailsActivity, PersonDetailsActivity::class.java)
            intent.putExtra("person", shortPerson)
            startActivity(intent)
        }

        listCourseStudentsButton?.setOnClickListener {
            LoadEnrolledPersonsTask().execute()
        }

        addStudentsButton?.setOnClickListener {
            val currentCourse = ShortCourse(course!!)
            val intent = Intent(this@CourseDetailsActivity, EditStudentsActivity::class.java)
            intent.putExtra("course", currentCourse)
            intent.putExtra("action", "Add")
            startActivity(intent)
        }

        removeStudentsButton?.setOnClickListener {
            val shortCourse = ShortCourse(course!!)
            val intent = Intent(this@CourseDetailsActivity, EditStudentsActivity::class.java)
            intent.putExtra("course", shortCourse)
            intent.putExtra("action", "Remove")
            startActivity(intent)
        }

    }

    private inner class LoadShortCourseTask: AsyncTask<ShortCourse, Void, Course?>() {

        override fun doInBackground(vararg sCourse: ShortCourse): Course? {
            val rest = RestFactory.instance
            return rest.getCourse(sCourse[0].id)
        }

        override fun onPostExecute(newCourse: Course?) {
            course = newCourse
            courseName?.text = course?.name
            courseDescription?.text = course?.description

            this@CourseDetailsActivity.teacher?.text = course?.teacher?.name
        }
    }

    private inner class LoadEnrolledPersonsTask: AsyncTask<Void, Void, List<ShortPerson>?>() {
        override fun doInBackground(vararg params: Void): List<ShortPerson>? {
            val rest = RestFactory.instance

            if(course != null){
                //Log.d("LECTURES_DEBUG", "course instance is not null!")
                return rest.getCourseStudents(course!!.id)
            }else {
                //Log.d("LECTURES_DEBUG", "course instance is null, returning all students!")
                return rest.getListOfPersons()
            }
        }

        override fun onPostExecute(persons: List<ShortPerson>?) {
            // invoked on UI thread so Toast in function below shouldn't cause an error
            updateEnrolledPersonsList(persons)
        }
    }

    private fun updateEnrolledPersonsList(persons: List<ShortPerson>?) {
        if(persons != null) {
            val adapter = EnrolledPersonAdapter(this,
                android.R.layout.simple_list_item_1, persons)
            courseStudentsListView?.adapter = adapter
        } else {
            Toast.makeText(applicationContext, "Enrolled students couldn't be loaded correctly!", Toast.LENGTH_LONG).show()
        }
    }

    private inner class EnrolledPersonAdapter(context: Context, textViewResourceId: Int, private val shortPersonList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortPersonList)
}
