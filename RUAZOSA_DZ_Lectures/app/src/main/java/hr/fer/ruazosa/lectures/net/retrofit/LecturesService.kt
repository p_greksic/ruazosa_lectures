package hr.fer.tel.ruazosa.lectures.net.retrofit


import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import retrofit.http.*

interface LecturesService {
    @get:GET("/courses")
    val listOfCourses: List<ShortCourse>

    @GET("/courses/{courseId}")
    fun getCourse(@Path("courseId") courseId: Long?): Course

    @GET("/courses/{courseId}/students")
    fun getCourseStudents(@Path("courseId") courseId: Long?): List<ShortPerson>

    // dodano
    @get:GET("/persons")
    val listOfPersons: List<ShortPerson>

    @GET("/persons/{personId}")
    fun getPerson(@Path("personId") personId: Long?): Person

    @POST("/courses/{courseId}/enrollPerson/{personId}")
    fun enrollPersonToCourse(@Path("courseId") courseId:Long?, @Path("personId") personId: Long?): Boolean?

    @POST("/courses/{courseId}/unenrollPerson/{personId}")
    fun disenrollPersonFromCourse(@Path("courseId") courseId: Long?, @Path("personId") personId: Long?): Boolean?

    @DELETE("/persons/{personId}")
    fun deletePerson(@Path("personId") personId: Long?): Boolean?

    @POST("/persons")
    fun postPerson(@Body person: Person?): Boolean?
}
