package hr.fer.ruazosa.lectures

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory

class MainActivity : AppCompatActivity() {

    private var itemsListView: ListView? = null
    private var loadCoursesButton: Button? = null
    private var loadPersonsButton: Button? = null
    private var loadCoursesButtonPressed: Boolean = false
    private var createPersonButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemsListView = findViewById(R.id.itemsListView) as ListView
        itemsListView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            if(loadCoursesButtonPressed) {
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortCourse = itemAtPosition as ShortCourse
                val intent = Intent(this@MainActivity, CourseDetailsActivity::class.java)
                intent.putExtra("course", shortCourse)
                startActivity(intent)
            }else{
                // when person is clicked start activity for editing their params or deleting them
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortPerson = itemAtPosition as ShortPerson
                val intent = Intent(this@MainActivity, PersonDetailsActivity::class.java)
                intent.putExtra("person", shortPerson)
                intent.putExtra("action", "Update/Delete")
                startActivity(intent)
            }
        }

        loadCoursesButton = findViewById(R.id.loadCoursesButton) as Button
        loadCoursesButton?.setOnClickListener {
            loadCoursesButtonPressed = true
            LoadCoursesTask().execute()
        }

        loadPersonsButton = findViewById(R.id.loadPersonsButton) as Button
        loadPersonsButton?.setOnClickListener {
            loadCoursesButtonPressed = false
            LoadPersonsTask().execute()
        }

        createPersonButton = findViewById(R.id.createPersonButton) as Button
        createPersonButton?.setOnClickListener {
            val shortPerson = ShortPerson()
            shortPerson.id = -1
            shortPerson.name = ""
            val intent = Intent(this@MainActivity, PersonDetailsActivity::class.java)
            intent.putExtra("person", shortPerson)
            intent.putExtra("action", "Create")
            startActivity(intent)
        }

    }


    private inner class LoadCoursesTask: AsyncTask<Void, Void, List<ShortCourse>?>() {
        override fun doInBackground(vararg params: Void): List<ShortCourse>? {
            val rest = RestFactory.instance

            return rest.getListOfCourses()
        }

        override fun onPostExecute(courses: List<ShortCourse>?) {
            // invoked on UI thread so Toast in function below shouldn't cause an error
            updateCourseList(courses)
        }
    }

    private fun updateCourseList(courses: List<ShortCourse>?) {
        if(courses != null) {
            val adapter = CourseAdapter(this,
                android.R.layout.simple_list_item_1, courses)
            itemsListView?.adapter = adapter
        } else {
            // TODO show that courses can not be loaded
            Toast.makeText(applicationContext, "Courses couldn't be loaded correctly!", Toast.LENGTH_LONG).show()
        }
    }

    private inner class CourseAdapter(context: Context, textViewResourceId: Int, private val shortCourseList: List<ShortCourse>) : ArrayAdapter<ShortCourse>(context, textViewResourceId, shortCourseList)

    private inner class LoadPersonsTask: AsyncTask<Void, Void, List<ShortPerson>?>() {
        override fun doInBackground(vararg params: Void): List<ShortPerson>? {
            val rest = RestFactory.instance

            return rest.getListOfPersons()
        }

        override fun onPostExecute(persons: List<ShortPerson>?) {
            // invoked on UI thread so Toast in function below shouldn't cause an error
            updatePersonsList(persons)
        }
    }

    private fun updatePersonsList(persons: List<ShortPerson>?) {
        if(persons != null) {
            val adapter = PersonAdapter(this,
                android.R.layout.simple_list_item_1, persons)
            itemsListView?.adapter = adapter
        } else {
            // TODO show that courses can not be loaded
            Toast.makeText(applicationContext, "Persons couldn't be loaded correctly!", Toast.LENGTH_LONG).show()
        }
    }

    private inner class PersonAdapter(context: Context, textViewResourceId: Int, private val shortPersonList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortPersonList)

}
