package hr.fer.ruazosa.lectures

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory

class PersonDetailsActivity : AppCompatActivity() {
    private var actionString: String? = null
    private var person: Person? = null
    private var firstNameEditText: EditText? = null
    private var lastNameEditText: EditText? = null
    private var roomEditText: EditText? = null
    private var phoneEditText: EditText? = null
    private var updatePersonButton: Button? = null
    private var deletePersonButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_details)

        firstNameEditText = findViewById(R.id.firstNameEditText) as EditText
        lastNameEditText = findViewById(R.id.lastNameEditText) as EditText
        roomEditText = findViewById(R.id.roomEditText) as EditText
        phoneEditText = findViewById(R.id.phoneEditText) as EditText
        updatePersonButton = findViewById(R.id.updatePersonButton) as Button
        deletePersonButton = findViewById(R.id.deletePersonButton) as Button

        val shortPerson = intent.getSerializableExtra("person") as ShortPerson
        val actionString = intent.getStringExtra("action") as String

        if(actionString.equals("Update/Delete")) {
            // then load Person from shortPerson if actionString is Update/Delete
            LoadPersonTask().execute(shortPerson)

            updatePersonButton?.setOnClickListener {
                // collect data from edit text boxes
                person?.firstName = firstNameEditText?.text.toString()
                person?.lastName = lastNameEditText?.text.toString()
                person?.phone = phoneEditText?.text.toString()
                person?.room = roomEditText?.text.toString()
                // execute update person task
                UpdatePersonTask().execute(person)
            }

            deletePersonButton?.setOnClickListener {
                DeletePersonTask().execute(person)
            }
        }else if(actionString.equals("Create")){

            deletePersonButton?.setOnClickListener {
                Toast.makeText(applicationContext,"Operation not supported!",Toast.LENGTH_LONG).show()
            }

            updatePersonButton?.text = resources.getString(R.string.create_person_button)

            updatePersonButton?.setOnClickListener {
                // collect data from edit text boxes
                person = Person()
                person?.firstName = firstNameEditText?.text.toString()
                person?.lastName = lastNameEditText?.text.toString()
                person?.phone = phoneEditText?.text.toString()
                person?.room = roomEditText?.text.toString()
                // execute update person task
                CreatePersonTask().execute(person)
            }
        }else{
            Log.d("LECTURES_DEBUG", "PersonDetailsActivity received wrong action string or no action string extra.")
            finish()
        }
    }

    private inner class LoadPersonTask: AsyncTask<ShortPerson, Void, Person?>() {

        override fun doInBackground(vararg sPerson: ShortPerson): Person? {
            val rest = RestFactory.instance
            return rest.getPerson(sPerson[0].id)
        }

        override fun onPostExecute(newPerson: Person?) {
            person = newPerson
            firstNameEditText?.setText(person?.firstName, TextView.BufferType.EDITABLE)
            lastNameEditText?.setText(person?.lastName, TextView.BufferType.EDITABLE)
            roomEditText?.setText(person?.room, TextView.BufferType.EDITABLE)
            phoneEditText?.setText(person?.phone, TextView.BufferType.EDITABLE)
        }
    }

    private inner class CreatePersonTask: AsyncTask<Person, Void, Boolean?>() {

        override fun doInBackground(vararg person: Person): Boolean? {
            val rest = RestFactory.instance

            person[0].id = null

            return rest.postPerson(person[0])
        }

        override fun onPostExecute(result: Boolean?) {
            if(result == null || result == false){
                Log.d("LECTURES_DEBUG", "CreatePersonTask in PersonDetailsActivity was unsuccessful!")
            }
            finish()
        }
    }

    private inner class UpdatePersonTask: AsyncTask<Person, Void, Boolean?>() {

        override fun doInBackground(vararg person: Person): Boolean? {
            val rest = RestFactory.instance

            return rest.updatePerson(person[0])
        }

        override fun onPostExecute(result: Boolean?) {
            if(result == null || result == false){
                Log.d("LECTURES_DEBUG", "UpdatePersonTask in PersonDetailsActivity was unsuccessful!")
            }
            finish()
        }
    }

    private inner class DeletePersonTask: AsyncTask<Person, Void, Boolean?>() {

        override fun doInBackground(vararg person: Person): Boolean? {
            val rest = RestFactory.instance

            return rest.deletePerson(person[0].id)
        }

        override fun onPostExecute(result: Boolean?) {
            if(result == null || result == false){
                Log.d("LECTURES_DEBUG", "DeletePersonTask in PersonDetailsActivity was unsuccessful!")
            }
            finish()
        }
    }
}