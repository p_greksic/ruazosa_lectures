package hr.fer.tel.ruazosa.lectures.net.inmemory

import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestInterface
import java.util.*

class InMemoryRestImplementation : RestInterface {
    private val persons: MutableList<Person>
    private val courses: MutableList<Course>
    private val enrolled: MutableMap<Long, MutableList<Person>> // CourseId, list of students

    init {
        persons = LinkedList()
        courses = LinkedList()
        enrolled = HashMap()

        var c = Course(1L, "RUAZOSA", "Razvoj usluga i aplikacija za operacijski sustav Android")
        courses.add(c)

        c = Course(2L, "KP", "Konkurentno programiranje")
        courses.add(c)

        c = Course(3L, "RASSUS", "Raspodijeljeni sustavi")
        courses.add(c)

        c = Course(4L, "OOP", "Objektno orijentirano programiranje")
        courses.add(c)

        var p = Person(1L, "Pero", "Perić", "1111", "C15-15")
        persons.add(p)
        getCourse(1L)?.teacher = ShortPerson(p)

        p = Person(2L, "Iva", "Ivić", "555", "C0-32")
        persons.add(p)
        enrollPersonToCourse(p.id, c.id)

        p = Person(3L, "Jura", "Jurić", "333", "C3-81")
        persons.add(p)
        enrollPersonToCourse(p.id, c.id)
        /*var retval = enrollPersonToCourse(p.id, c.id)
        if(retval == null){
            Log.d("LECTURES_DEBUG", "retval is null")
        }*/
    }

    override fun getListOfCourses(): List<ShortCourse>? {
            val result = LinkedList<ShortCourse>()
            for (c in courses)
                result.add(ShortCourse(c))

            return result
    }

    override fun getCourse(id: Long?): Course? {
        for (c in courses)
            if (c.id == id) return c
        return null
    }

    override fun getCourseStudents(courseId: Long?): List<ShortPerson> {
        val result = LinkedList<ShortPerson>()
        val enrolledPersonList = enrolled[courseId]
        //Log.d("LECTURES_DEBUG", "courseID: " + courseId.toString())
        //Log.d("LECTURES_DEBUG", "enrolledPersonList size: " + enrolledPersonList!!.size.toString())
        if (enrolledPersonList != null) {
            for (p in enrolledPersonList)
                result.add(ShortPerson(p))
        }

        return result
    }

    override fun getListOfPersons(): List<ShortPerson>? {
            val result = LinkedList<ShortPerson>()
            for (p in persons)
                result.add(ShortPerson(p))
            return result
    }

    override fun getPerson(id: Long?): Person? {
        for (p in persons)
            if (p.id == id) return p
        return null
    }

    override fun enrollPersonToCourse(personId: Long?, courseId: Long?): Boolean? {
        if(courseId != null && personId != null) {
            var personsEnrolled = enrolled.get(courseId)
            var person = getPerson(personId)
            if (person != null) {
                if(personsEnrolled == null){
                    personsEnrolled = mutableListOf()
                }
                if(personsEnrolled.contains(person)){
                    return true
                }
                personsEnrolled.add(person)
                enrolled[courseId] = personsEnrolled
                return true
            }
            return null
        }

        return false
    }

    override fun disenrollPersonFromCourse(personId: Long?, courseId: Long?): Boolean? {
        val peronsEnrolled = enrolled[courseId] ?: return true

        for (p in peronsEnrolled) {
            if (p.id == personId)
                peronsEnrolled.remove(p)
        }
        return true
    }

    override fun deletePerson(id: Long?): Boolean? {
        var p_ind: Int = 0 // index of person to remove in persons
        var personToDelete: Person? = null

        if(id != null){
            // find index of person with given id
            for(i in 0 until persons.size){
                if(id == persons[i].id){
                    p_ind = i
                    break
                }
            }

            //fetch person for disenroll checks
            personToDelete = persons[p_ind]

            // disenroll person from all courses
            for( c in courses){
                if(enrolled[c.id] != null) {
                    if(enrolled[c.id]!!.contains(personToDelete)) {
                        disenrollPersonFromCourse(id, c.id)
                    }
                }
            }

            // finally remove person from persons
            persons.removeAt(p_ind)
            return true
        }
        // return false if null object given
        return false
    }

    override fun postPerson(p: Person?): Boolean? {
        var nextId: Long = 0
        if(p != null){
            for(person in persons){
                if(person.id!! >= nextId){
                    nextId = person.id as Long
                }
            }
            nextId++
            p.id = nextId
            persons.add(p)
            return true
        }
        // return false if null object given
        return false
    }

    override fun updatePerson(p: Person?): Boolean? {
        var insertIndex: Int = 0
        if(p != null){
            for( i in 0 until persons.size){
                if(persons[i].id == p.id){
                    insertIndex = i
                    break
                }
            }
            //replace "old" person
            persons[insertIndex] = p
            return true
        }else{
            return false
        }
        // this line shouldn't be reached
        return false
    }
}
