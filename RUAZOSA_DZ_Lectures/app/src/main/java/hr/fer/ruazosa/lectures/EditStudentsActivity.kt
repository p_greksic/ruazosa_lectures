package hr.fer.ruazosa.lectures

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory

class EditStudentsActivity : AppCompatActivity() {
    private var actionString: String? = null
    private var affectedCourse: ShortCourse? = null
    private var selectedShortPersons: MutableList<ShortPerson> = mutableListOf()
    private var editActionButton: Button? = null
    private var itemsListView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_students)

        actionString = intent.getStringExtra("action") as String
        affectedCourse = intent.getSerializableExtra("course") as ShortCourse

        editActionButton = findViewById(R.id.editActionButton) as Button
        itemsListView = findViewById(R.id.itemsListView) as ListView

        if(actionString.equals("Add")){
            // activity was called for adding students to course
            editActionButton?.setText(R.string.add_students_button, TextView.BufferType.EDITABLE)
            itemsListView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
                // when person is clicked add them to selectedPersons and change their color
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortPerson = itemAtPosition as ShortPerson
                if(selectedShortPersons.contains(shortPerson)){
                    // if person was in list remove them
                    selectedShortPersons.remove(shortPerson)
                    Toast.makeText(applicationContext,shortPerson.toString() + " was removed from selection.", Toast.LENGTH_LONG).show()
                }else{
                    // if person was not in list add them
                    selectedShortPersons.add(shortPerson)
                    Toast.makeText(applicationContext,shortPerson.toString() + " was added to selection.", Toast.LENGTH_LONG).show()
                }
            }

            editActionButton?.setOnClickListener {
                // add selected students from course
                val taskBundle = StudentsActionBundle(selectedShortPersons,affectedCourse)
                AddSelectedPersonsTask().execute(taskBundle)
            }

            LoadNotEnrolledPersonsTask().execute(affectedCourse)
        }else if(actionString.equals("Remove")){
            // activity was called for removing students from the course
            editActionButton?.setText(R.string.remove_students_button, TextView.BufferType.EDITABLE)
            itemsListView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
                // when person is clicked add them to selectedPersons and change their color
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortPerson = itemAtPosition as ShortPerson
                if(selectedShortPersons.contains(shortPerson)){
                    // if person was in list remove them
                    selectedShortPersons.remove(shortPerson)
                    Toast.makeText(applicationContext,shortPerson.toString() + " was removed from selection.", Toast.LENGTH_LONG).show()
                }else{
                    // if person was not in list add them
                    selectedShortPersons.add(shortPerson)
                    Toast.makeText(applicationContext,shortPerson.toString() + " was added to selection.", Toast.LENGTH_LONG).show()
                }
            }

            editActionButton?.setOnClickListener {
                // remove selected students from course
                val taskBundle = StudentsActionBundle(selectedShortPersons,affectedCourse)
                RemoveSelectedPersonsTask().execute(taskBundle)
            }

            LoadEnrolledPersonsTask().execute(affectedCourse)
        }else{
            // something went wrong finish activity
            Log.d("LECTURES_DEBUG", "EditStudentsActivity received wrong action string or no action string extra.")
            finish()
        }
    }

    private inner class StudentsActionBundle(selectedStudents: List<ShortPerson>?, affectedCourse: ShortCourse?){
        var selectedStudents: List<ShortPerson>? = null
        private set
        var affectedCourse: ShortCourse? = null
        private set

        init {
            this.selectedStudents = selectedStudents
            this.affectedCourse = affectedCourse
        }
    }

    private inner class AddSelectedPersonsTask: AsyncTask<StudentsActionBundle, Void, Boolean>() {

        override fun doInBackground(vararg bundle: StudentsActionBundle): Boolean?{
            val rest = RestFactory.instance
            val studentsToAdd = bundle[0].selectedStudents
            val course = bundle[0].affectedCourse
            if(course != null && studentsToAdd != null){
                for (s in studentsToAdd) {
                    rest.enrollPersonToCourse(s.id, course.id)
                }
                return true
            }else if(course != null){
                // no students were selected but there is an affectedCourse
                return true
            }
            return false
        }

        override fun onPostExecute(result: Boolean?) {
            if(result != true){
                Log.d("LECTURES_DEBUG","AddSelectedPersonsTask didn't complete correctly!")
            }
            finish()
        }
    }

    private inner class RemoveSelectedPersonsTask: AsyncTask<StudentsActionBundle, Void, Boolean?>() {

        override fun doInBackground(vararg bundle: StudentsActionBundle): Boolean?{
            val rest = RestFactory.instance
            val studentsToRemove = bundle[0].selectedStudents
            val course = bundle[0].affectedCourse
            if( studentsToRemove != null && course != null) {
                for (s in studentsToRemove) {
                    rest.disenrollPersonFromCourse(s.id, course.id)
                }
                return true
            }else if(course != null){
                // no students were selected but there is an affectedCourse
                return true
            }
            return false
        }

        override fun onPostExecute(result: Boolean?) {
            if(result != true){
                Log.d("LECTURES_DEBUG","RemoveSelectedPersonsTask didn't complete correctly!")
            }
            finish()
        }
    }

    private inner class LoadEnrolledPersonsTask: AsyncTask<ShortCourse, Void, List<ShortPerson>?>() {

        override fun doInBackground(vararg sCourse: ShortCourse): List<ShortPerson>?{
            val rest = RestFactory.instance
            return rest.getCourseStudents(sCourse[0].id)
        }

        override fun onPostExecute(result: List<ShortPerson>?) {
            updatePersonsList(result)
        }
    }

    private inner class LoadNotEnrolledPersonsTask: AsyncTask<ShortCourse, Void, List<ShortPerson>?>() {

        override fun doInBackground(vararg sCourse: ShortCourse): List<ShortPerson>?{
            val rest = RestFactory.instance
            val enrolledStudents = rest.getCourseStudents(sCourse[0].id)
            val allStudents = rest.getListOfPersons()
            val result:  MutableList<ShortPerson>? = mutableListOf()
            if(enrolledStudents == null){
                return allStudents
            }else if(enrolledStudents != null && allStudents != null){
                for( s in allStudents){
                    if(!enrolledStudents.contains(s)){
                        result!!.add(s)
                    }
                }
            }
            return result as List<ShortPerson>
        }

        override fun onPostExecute(result: List<ShortPerson>?) {
            updatePersonsList(result)
        }
    }

    private fun updatePersonsList(persons: List<ShortPerson>?) {
        if(persons != null) {
            val adapter = PersonAdapter(this,
                android.R.layout.simple_list_item_1, persons)
            itemsListView?.adapter = adapter
        } else {
            // TODO show that courses can not be loaded
            Toast.makeText(applicationContext, "Persons couldn't be loaded correctly!", Toast.LENGTH_LONG).show()
        }
    }

    private inner class PersonAdapter(context: Context, textViewResourceId: Int, private val shortPersonList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortPersonList)
}